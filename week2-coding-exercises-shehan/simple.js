//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
//question5(); 

//question 5 is complete and please uncomment to view it


function question1(){
    let output = "";
       // Given array of numbers
    let Given=[54,-16,80,55,-74,73,26,5,-34,-73,19,63,-55,-61,65,-14,-19,-51,-17,-25]
    
    let pos_odd=[];
    let neg_even=[];
    //Allocating data to relevent arrays
    for (i of Given)
	{
		if (i>0)
		{
			if (i % 2 !==0)
				pos_odd.push(i)
		}
		else
		{
			if (i % 2 ===0)
				neg_even.push(i)
		}

	}
        output = "Question 1\n\n" + "Positive Odd: " + pos_odd + "\nNegative Even: " + neg_even //empty output, fill this so that it can print onto the page.
    
let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here 
    
    
    function randBetween(min, max) {
  return Math.floor((Math.random() * 6) + 1);
}

  
let one = 0;
let two = 0;
let three = 0;
let four = 0;
let five = 0;
let six = 0;

for (let i = 0; i < 60000; i++) {
  const n = randBetween(1, 6);
  if (n === 1) { one++; }
  else if (n === 2) { two++; }
  else if (n === 3) { three++; }
  else if (n === 4) { four++; }
  else if (n === 5) { five++; }
  else { six++; }
}
  
output+=("Frequency of die rolls")
    output += "\n";

output+=("1: "+ one);
     output += "\n";
    output+=("2: "+two);
     output += "\n";
    output+=("3: "+three);
     output += "\n";
    output+=("4: "+four);
     output += "\n";
    output+=("5: "+five);
     output += "\n";
    output+=("6: "+six);
    
    
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here 
    
   var freqs = [1,2,3,4,5,6,7];
    
var freqsLength = freqs.length; //* save array length into variable for more productivity
var rolls = 60000; //* how many time we want to roll dice

for (let i = 0; i < 60000; i++) { //* start rolling in loop
  var j = Math.floor((Math.random() * 6) + 1); //* get random value
  freqs[j]++; //* save freq result
}
    
output+=("Frequency of die rolls")
    output += "\n";

output+=("1: "+ freqs[1]);
     output += "\n";
    output+=("2: "+freqs[2]);
     output += "\n";
    output+=("3: "+freqs[3]);
     output += "\n";
    output+=("4: "+freqs[4]);
     output += "\n";
    output+=("5: "+freqs[5]);
     output += "\n";
    output+=("6: "+freqs[6]);    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    
    
    var dieRolls = {    //Declaring the object
    Frequencies: {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0
    },
    Total: 60000,
    Exceptions: ""
}

for(let i=0;i<dieRolls.Total;i++){                                      
    //Iterating Total number of count times
    dieRolls.Frequencies[Math.floor((Math.random() * 6) + 1)]++;        
    // Increment the frequency of generated number
}

expectedValue = dieRolls.Total / 6;                                     
    // Expected value should be total / 6
onePercent = expectedValue / 100;                                       
    // 1% of expectedValue

for(let i=1;i<=6;i++){
    if(Math.abs(dieRolls.Frequencies[i]-expectedValue) > onePercent)   
        // If Absolute difference is greater than onePercent
        dieRolls.Exceptions += i + ",";                                 
    // This should be included in the exception string
}
dieRolls.Exceptions = dieRolls.Exceptions.substring(0,dieRolls.Exceptions.length-1); 
    // Remove extra delimiter
    
// Printing all the values
output += ("Frequency of dice rolls");
    output += "\n";
output+= ("Total rolls: "+dieRolls.Total);
    output += "\n";
output += ("Frequencies: "+dieRolls.Frequencies);
    output += "\n";
output += ("Exceptions: "+dieRolls.Exceptions);
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
    //question 5 is complete and please uncomment to view it
    
    
    window.addEventListener('load', function() {    // This function loads when window loads
        //main code starts here
        let person = {                              //person object
            name : "",
            income : 0
        }
        person.name = prompt("Enter your name");                //taking user input for name
        person.income = parseInt(prompt("Enter your Income"));  //taking user input for income

        let tax = parseFloat(0.00);                             //initialising tax

        if (person.income <= 18200)                             //if income is $0 - $18000 tax is nill
            tax = parseFloat(0.00);
        else if (person.income <= 37000) {                      //if income is $18001 - $37000 tax is calculated as asked
            let taxableIncome = person.income - 18000;
            tax = taxableIncome * 0.19;
        }
        else if (person.income <= 90000) {                      //if income is $37001 - $90000 tax is calculated as asked
            let taxableIncome = person.income - 37000;
            tax = taxableIncome * 0.325;
            tax = tax + 3572;
        }
        else if (person.income <= 180000){                      //if income is $90001 - $180000 tax is calculated as asked
            let taxableIncome = person.income - 90000;
            tax = taxableIncome * 0.37;
            tax = tax + 20797;
        }else {                                                 //if income is $180001 and over tax is calculated as asked
            let taxableIncome = person.income - 180000;
            tax = taxableIncome * 0.45;
            tax = tax + 54097;
        }
        //Displaying output
        document.write(person.name+"'s income is: $"+ person.income+", and his/her tax owed is: "+tax.toFixed(2));
    })
    
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}